function [flagsom,stemax] = analisasepara(x,stemaxaux,cont)

flagsom = [];

xn = x/max(x);
ste = sum(xn.^2);

if cont == 1
    stemax = ste;
else
    if ste > stemaxaux && isnan(ste) ~= 1
        stemax = ste;
    else
        stemax = stemaxaux;
    end
end

% Trocar 0.6 por 0.01 (1%)
if ste/stemax < 0.01 && isnan(ste) ~= 1
    flagsom = 1;
elseif isnan(ste) == 1 && stemax ~= 1
    flagsom = 1;
else
    return
end