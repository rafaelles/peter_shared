clear all
close all
clc

fSize = 24;

%%
% Ver graficamente o que acontece quando o criterio de Nyquist na escolha
% de frequencia de amostragem

% Criando 3 sinais senoidais com frequencias diferentes dadas por f1, f2 e
% f3

f1 = 5;         %Hz
f2 = 10;        %Hz
f3 = 20;        %Hz

% Frequencia de amostragem, Fs (Hz) ou ws (rad/s). � preferivel trabalhar
% com frequencia na unidade mais natural que � (rad/s).
ws = 35;    %rad/s

% Constru��o do vetor de tempo
dt = 1/ws;      %s
time = 0:dt:5;

x1 = sin(2*pi*f1*time);
x2 = sin(2*pi*f2*time);
x3 = sin(2*pi*f3*time);

% Dominio da frequencia
freq = linspace(-ws/2,ws/2,length(x1));

% Calculando FFT dos sinais
X1 = fft(x1);
X2 = fft(x2);
X3 = fft(x3);

X1s = fftshift(X1);
X2s = fftshift(X2);
X3s = fftshift(X3);

%
figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
h1 = plot(freq,abs(X1s)/max(abs(X1s)),'-k',...
    freq,abs(X2s)/max(abs(X2s)),'-r',...
    freq,abs(X3s)/max(abs(X3s)),'-b');
grid on;
xlabel('$f (Hz)$','interpreter','latex','fontsize',fSize);
ylabel('$|X(\omega)|$','interpreter','latex','fontsize',fSize);
leg1 = legend('$|X_{1}(\omega)|$','$|X_{2}(\omega)|$',...
    '$|X_{3}(\omega)|$');
set(gca,'fontsize',fSize);
set(leg1,'fontsize',20,'location','eastoutside',...
    'interpreter','latex');
set(h1,'linewidth',1.5);
axis([-ws/2 ws/2 0 1]);

saveas(gcf,'Espectro1','png');

%%
input('Pressione ENTER para continuar...');
close all

% Levando em conta de o crit�rio de Nyquist
% (fazendo ws > 2*wmax ou Fs > 2*fmax)

wmax = max(2*pi*[f1 f2 f3]);
ws2 = 5*wmax;
dt2 = 1/ws2;
time2 = 0:dt2:5;
freq2 = linspace(-ws2/2,ws2/2,length(time2));

x12 = sin(2*pi*f1*time2);
x22 = sin(2*pi*f2*time2);
x32 = sin(2*pi*f3*time2);

X12 = fft(x12);
X22 = fft(x22);
X32 = fft(x32);

X12s = fftshift(X12);
X22s = fftshift(X22);
X32s = fftshift(X32);

% Fazendo um grafico que compara o resultado levando em conta (ou n�o)
% o criterio de Nyquist

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
h2 = plot(freq,abs(X1s)/max(abs(X1s)),'--k',...
    freq,abs(X2s)/max(abs(X2s)),'--r',...
    freq,abs(X3s)/max(abs(X3s)),'--b');
set(h2,'linewidth',1.5);
hold on;
h3 = plot(freq2,abs(X12s)/max(abs(X12s)),'-k',...
    freq2,abs(X22s)/max(abs(X22s)),'-r',...
    freq2,abs(X32s)/max(abs(X32s)),'-b');
set(h3,'linewidth',1.5);
grid on;
xlabel('$f (Hz)$','interpreter','latex','fontsize',fSize);
ylabel('$|X(\omega)|$','interpreter','latex','fontsize',fSize);
leg2 = legend('$|X_{1}(\omega)|$',...
    '$|X_{2}(\omega)|, Fs < 2f_{max}$',...
    '$|X_{3}(\omega)|$',...
    '$|X_{1}(\omega)|$',...
    '$|X_{2}(\omega)|, Fs > 2f_{max}$',...
    '$|X_{3}(\omega)|$');
set(gca,'fontsize',fSize);
set(leg2,'fontsize',20,'location','eastoutside',...
    'interpreter','latex');
axis([-25 25 0 1]);

saveas(gcf,'Espectro2','png');
