clear all
close all
clc

% Parâmetro de tamanho de fonte para os gráficos
fSize = 24;

%%
% Para estudar o processo de filtragem, vou supor que iremos usar um sinal
% de áudio que eu criei falando BA BI

% Carregando o áudio
x = audioread('BaBi.wav');

% Buscando informações sobre o áudio, em especial a frequência de
% amostragem (Fs)
xinfo = audioinfo('BaBi.wav');

Fs = xinfo.SampleRate;      % Hz

% Ouvindo o áudio
sound(x,Fs);
pause(2);

% Criando o vetor de tempo do sinal a partir do conhecimento de que o sinal
% de áudio começa em 0 e tem frequência de amostragem Fs
time = linspace(0,length(x)/Fs,length(x));

% Registro temporal do sinal de áudio
figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
plot(time,x);
grid on;
xlabel('$ t (s) $','interpreter','latex','fontsize',fSize);
ylabel('$ x(t) $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);

%%
pause(2);
close all

% Criando os vetores de frequência a partir do conhecimento da Fs e do
% números de pontos existentes no sinal (length(x))

freq = linspace(0,Fs,length(x));

% Para mostrar apenas metade do espectro da FFT, existe uma convenção:
% Se o número de elementos em length(x) for ímpar, usa o arrendodamento pra
% baixo de length(x)/2+1. Exemplo: floor(5/2)+1 = floor(2.5)+1 = 2+1 = 3
% Se, por outro lado, o número de elementos em x for par, apenas
% length(x)/2
if mod(length(x),2) ~= 0
    freq2 = linspace(0,Fs/2,floor(length(x)/2)+1);
else
    freq2 = linspace(0,Fs/2,length(x)/2);
end

% Criando um vetor que possa ser usado para um gráfico em que o espectro da
% FFT é centrado no zero
freq3 = linspace(-Fs/2,Fs/2,length(x));

% Calculando a FFT do sinal
X = fft(x);

% Reordenando a FFT calculado anteriormente para ficar centrado no zero
Xs = fftshift(X);

figure('units','normalized','outerposition',[0 0 0.98 0.98],'color','w');
subplot(2,2,1)
plot(freq/Fs,abs(X)/max(abs(X)),'-k');
grid on;
xlabel('$f/Fs $','interpreter','latex','fontsize',fSize);
ylabel('$ \left| X(\omega) \right|_{norm} $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);

subplot(2,2,[2 4])
plot(freq3/Fs,abs(Xs)/max(abs(Xs)),'-k');
grid on;
xlabel('$f/Fs $','interpreter','latex','fontsize',fSize);
ylabel('$ \left| X(\omega) \right|_{norm} $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);

subplot(2,2,3)
plot(freq2/Fs,abs(X(1:length(freq2)))/max(abs(X)),'-k');
grid on;
xlabel('$f/Fs $','interpreter','latex','fontsize',fSize);
ylabel('$ \left| X(\omega) \right|_{norm} $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([0 0.5 0 1]);

%%
pause(2);
close all

% Filtro passa baixa
% Irei cortar qualquer frequência acima de f >= fcut (Hz)

fcut = 1000;         % Hz

% Com o espectro que é centrado no zero fica mais fácil!
% Quero encontrar quais são os índices do vetor de frequência (freq3) que
% tem frenquência >= fcut e <= fcut (para anular todos os pares de
% frequências que sejam acima de fcut (lembrar que o lado negativo aparece
% por conta do conjugado no número complexo)

indfil = find(abs(freq3)>=fcut);

% Usando o espectro centrado em zero e zerando todas frequências acima de
% fcut
Xsfil=Xs;
Xsfil(indfil) = 0;

% Voltando para o sinal que não está centrado em zero, mas em Fs/2
Xfil = ifftshift(Xsfil);

figure('units','normalized','outerposition',[0 0 0.98 0.98],'color','w');
subplot(2,2,1)
plot(freq/Fs,abs(X)/max(abs(X)),'-k',...
    freq/Fs,abs(Xfil)/max(abs(Xfil)),'-r');
grid on;
xlabel('$f/Fs $','interpreter','latex','fontsize',fSize);
ylabel('$ \left| X(\omega) \right|_{norm} $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);

subplot(2,2,[2 4])
plot(freq3/Fs,abs(Xs)/max(abs(Xs)),'-k',...
    freq3/Fs,abs(Xsfil)/max(abs(Xsfil)),'-r');
grid on;
xlabel('$f/Fs $','interpreter','latex','fontsize',fSize);
ylabel('$ \left| X(\omega) \right|_{norm} $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
legend('Original','Filtered');

subplot(2,2,3)
plot(freq2/Fs,abs(X(1:length(freq2)))/max(abs(X)),'-k',...
    freq2/Fs,abs(Xfil(1:length(freq2)))/max(abs(Xfil)),'-r');
grid on;
xlabel('$f/Fs $','interpreter','latex','fontsize',fSize);
ylabel('$ \left| X(\omega) \right|_{norm} $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([0 0.5 0 1]);

%%
pause(2);
close all
% Removendo componentes de frequência maior do que fcut e voltando para o
% domínio do tempo para obter o sinal de áudio filtrado

xfil = real(ifft(Xfil));
% Teoricalmente, cancelando os pares conjugados de frequências, a gente
% obtém um sinal real. Porém existe erro numérico, então coloca-se esse
% real(...) para extrair somente a parte real do sinal xfil. O máximo valor
% de número imaginário no teste está por volta de 1x10⁵, ou seja, 0.

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
plot(time,x,'-k',time,xfil,'-r');
grid on;
xlabel('$ t (s) $','interpreter','latex','fontsize',fSize);
ylabel('$ x(t) $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
legend('Original','Filtered');

%%
pause(2);
close all

% Salvando o novo áudio filtrado
audiowrite('BaBi_filt.wav',xfil,Fs);

% Tocando o áudio filtrado
sound(xfil,Fs);

% Comparando com o sinal de áudio original
pause(2);
sound(x,Fs);