clear all
close all
clc
%% Registro de audio - Banco de dados (Experimento)
load('Som.mat')
%% audio
interval=2;
fs=44100;
%% vari�veis
ft=0;
num_tent=2400;
%%
for n=1:num_tent
    if ft<1 || ft>24
        ft=1;
        cond_rand=randperm(24);
    end
    if n~=1 && ft==1
        fprintf('\n Continuar por mais uma rodada?\n')
        Quest=input('     (1)sim   ou   (2)n�o \n');
        if Quest==2
            break
        else
            clc
            pause(1)
        end
    end
    cond=Som(cond_rand(ft)).name;
    cod=cond_rand(ft);
    ft=ft+1;
   %%
    fprintf('\n                         fale o som:   %s\n',cond);
    fprintf('\n                    Pressione enter para gravar.\n');
    pause;
    clc
    aud = audiorecorder(fs, 8, 1);
    recordblocking(aud, interval);
    arq = aud.getaudiodata();
    Som(cod).audio(:,size(Som(cod).audio,2)+1)=arq;
 
end

journal_som=sprintf('Som.mat');
eval(sprintf('save %s Som',journal_som));