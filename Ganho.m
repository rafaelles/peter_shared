clear all
close all
clc

% Parâmetro de tamanho de fonte para os gráficos
fSize = 24;

%%
% Gerando um sinal qualquer com parâmetro de frequência de amostragem, Fs,
% que é gerado num intervalo de 5s
Fs = 100;
dt = 1/Fs;

time = 0:dt:5;

% Sinal sintetizado
x = 0.2*exp(-0.0075*20*pi*2*time).*(sin(2*pi*time) + 0.75*sin(2*pi*5*time) - 0.8*sin(2*pi*8*time) + ...
    0.25*sin(2*pi*32*time)) + 0.02*rand(size(time));

% Registro temporal do sinal gerado
figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
plot(time,x);
grid on;
xlabel('$ t (s) $','interpreter','latex','fontsize',fSize);
ylabel('$ x(t) $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);

%%
pause(2);
close all

% Para mostrar apenas metade do espectro da FFT, existe uma convenção:
% Se o número de elementos em length(x) for ímpar, usa o arrendodamento pra
% baixo de length(x)/2+1. Exemplo: floor(5/2)+1 = floor(2.5)+1 = 2+1 = 3
% Se, por outro lado, o número de elementos em x for par, apenas
% length(x)/2
if mod(length(x),2) ~= 0
    freq2 = linspace(0,Fs/2,floor(length(x)/2)+1);
else
    freq2 = linspace(0,Fs/2,length(x)/2);
end

% Calculando a FFT do sinal
X = fft(x);


figure('units','normalized','outerposition',[0 0 0.6 0.6],'color','w');
plot(freq2,abs(X(1:length(freq2)))/max(abs(X)),'-k');
grid on;
xlabel('$f (Hz) $','interpreter','latex','fontsize',fSize);
ylabel('$ \left| X(\omega) \right|_{norm} $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);

%%
pause(2);
close all

% Calcular o ganho e a fase do sinal em dB
% Gain (dB) = 20*log10(abs(X))
% fase (rad) = atan(imag(X)/real(X)) = angle(X)

XdB = 20*log10(abs(X));
Xphs = (angle(X));


figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(2,1,1)
plot(freq2,XdB(1:length(freq2)),'-k');
grid on;
ylabel('$ Gain (dB) $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([0 50 -80 50])

subplot(2,1,2)
plot(freq2,Xphs(1:length(freq2))*180/pi,'-k');
grid on;
xlabel('$f (Hz)$','interpreter','latex','fontsize',fSize);
ylabel('$ \theta (^{\circ}) $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([0 50 -200 200])

%%
input('Precione ENTER para continuar...');
clearvars -except fSize Fs dt
close all

fprintf('\nRepetindo a análise anterior para o caso de uma onda quadrada');

% Gerando uma onda quadrada de período 2*pi com frequência de amostragem, 
% Fs, num intervalo de 5s

time = 0:dt:30;

% Sinal sintetizado
x = square(time);

% Registro temporal do sinal gerado
figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
plot(time,x);
grid on;
xlabel('$ t (s) $','interpreter','latex','fontsize',fSize);
ylabel('$ x(t) $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([0 30 -2 2]);

%%
pause(2);
close all

% Para mostrar apenas metade do espectro da FFT, existe uma convenção:
% Se o número de elementos em length(x) for ímpar, usa o arrendodamento pra
% baixo de length(x)/2+1. Exemplo: floor(5/2)+1 = floor(2.5)+1 = 2+1 = 3
% Se, por outro lado, o número de elementos em x for par, apenas
% length(x)/2
if mod(length(x),2) ~= 0
    freq2 = linspace(0,Fs/2,floor(length(x)/2)+1);
else
    freq2 = linspace(0,Fs/2,length(x)/2);
end

% Calculando a FFT do sinal
X = fft(x);


figure('units','normalized','outerposition',[0 0 0.6 0.6],'color','w');
plot(freq2,abs(X(1:length(freq2)))/max(abs(X)),'-k');
grid on;
xlabel('$f (Hz) $','interpreter','latex','fontsize',fSize);
ylabel('$ \left| X(\omega) \right|_{norm} $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([0 10 0 1]);

%%
pause(2);
close all

% Calcular o ganho e a fase do sinal em dB
% Gain (dB) = 20*log10(abs(X))
% fase (rad) = atan(imag(X)/real(X)) = angle(X)

XdB = 20*log10(abs(X));
Xphs = (angle(X));


figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(2,1,1)
plot(freq2,XdB(1:length(freq2)),'-k');
grid on;
ylabel('$ Gain (dB) $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([0 50 -80 50])

subplot(2,1,2)
plot(freq2,Xphs(1:length(freq2))*180/pi,'-k');
grid on;
xlabel('$f (Hz)$','interpreter','latex','fontsize',fSize);
ylabel('$ \theta (^{\circ}) $','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([0 50 -200 200])

