clear all
close all
clc

fontS = 18;

Fs = 512;
ti = 0;
tf = 4;
dt = 1/Fs;

time = ti:dt:tf; N = length(time);
freq1 = linspace(0,Fs,N);
freq2 = linspace(-Fs/2,Fs/2,N);

f = linspace(0,2,N);

f3 = f.^2;

x = sin(2*pi*f3.*time);

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(2,2,1);
plot(time,x); grid on;
xlabel('$t (s)$','interpreter','latex','fontsize',fontS);
ylabel('$x(s)$','interpreter','latex','fontsize',fontS);
axis([ti tf -1 1]);

subplot(2,2,3);
plot(time,f3); grid on;
xlabel('$t (s)$','interpreter','latex','fontsize',fontS);
ylabel('$f (Hz)$','interpreter','latex','fontsize',fontS);
axis([ti tf 0 4]);

subplot(2,2,2);
plot(freq2/Fs,fftshift(abs(fft(x)))/max(abs(fft(x)))); grid on;
xlabel('$f/F_{s}$','interpreter','latex','fontsize',fontS);
ylabel('$|X(\omega)|$','interpreter','latex','fontsize',fontS);

subplot(2,2,4);
semilogx(freq2,fftshift(20*log10(abs(fft(x)))/max(abs(fft(x))))); grid on;
xlabel('$f (Hz)$','interpreter','latex','fontsize',fontS);
ylabel('$|X(\omega)| dB$','interpreter','latex','fontsize',fontS);

close all;

Nwin = 777;
% Win = tukeywin(Nwin,2);
Win = kaiser(Nwin);

for ii = 1:length(x)
    if ii <= ceil(Nwin/2)
        auxwin = ceil(Nwin/2)-ii+1:Nwin;
        xwinaux = x(1:length(auxwin)).*Win(auxwin)';
        xwin(:,ii) = [xwinaux zeros(1,length(x(length(xwinaux)+1:end)))];
    elseif ii > length(x)-ceil(Nwin/2)
        auxwin = ii+1-ceil(Nwin/2):length(x);
        xwin(:,ii) = [zeros(1,length(x)-length(auxwin)) ...
            x(ii+1-ceil(Nwin/2):end).*Win(1:length(auxwin))'];
    else
        auxwin = ii+1-ceil(Nwin/2):ii+ceil(Nwin/2)-1;
        xwin(:,ii) = [zeros(1,length(x(1:auxwin(1)-1))) ...
            x(auxwin).*Win' ...
            zeros(1,length(x(auxwin(end)+1:end)))];
    end
end


figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w'); 
subplot(2,2,1)
plot(xwin(:,1:50))

subplot(2,2,2)
plot(xwin(:,200:250))

subplot(2,2,3)
plot(xwin(:,350:400))

subplot(2,2,4)
plot(xwin(:,500:513))

Xwin = fft(xwin);
for ii = 1:N
    auxXwin = Xwin(:,ii).*conj(Xwin(:,ii));
    auxXwin = auxXwin';
    SXwin(:,ii) = 2*sqrt(auxXwin)/N;
    AXwin(:,ii) = 2*auxXwin/N^2;
end

close all

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w'); 
pcolor(time,freq1/2,AXwin)
shading(gca,'interp');
colormap jet
hc=colorbar('eastoutside');
set(get(hc,'ylabel'),'string','$|X(\omega)|$','fontsize',14,'interpreter','Latex');
axis([0 4 0 8])
xlabel('$t (s)$','interpreter','latex','fontsize',fontS);
ylabel('$f (Hz)$','interpreter','latex','fontsize',fontS);

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w'); 
spectrogram(x,Win,10,512,Fs,'reassigned','yaxis')
axis([0 4 0 10])

 t=0:0.001:2;                    % 2 secs @ 1kHz sample rate
      y=chirp(t,100,1,200,'q');       % Start @ 100Hz, cross 200Hz at t=1sec
      spectrogram(y,kaiser(128,18),120,128,1E3,'yaxis');
      title('Quadratic Chirp: start at 100Hz and cross 200Hz at t=1sec');