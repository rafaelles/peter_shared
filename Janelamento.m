clear all
close all
clc

% Parâmetro de tamanho de fonte para os gráficos
fSize = 24;

% Tamanho do janelamento (pontos)
Nwin = 400;

%%
fprintf('Janela retangular\n\n');

% Criando a janela
wrect = [zeros(50,1); rectwin(Nwin); zeros(50,1)];

% Calculando o espectro de frequência
Wrect = fft(wrect);

% Calculando o ganho (dB)
WrdB = -20*log10(abs(Wrect));

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(2,2,[1 3])
plot(wrect);
grid on;
axis([0 500 0 1.05]);
title('Janela retangular');

subplot(2,2,[2 4])
plot(WrdB);
grid on;
ylabel('$Gain (dB)$','interpreter','latex','fontsize',fSize);

%%
input('Precione ENTER para continuar...');

fprintf('\n\nJanela Hann\n\n');

% Criando a janela
whann = [zeros(50,1); hann(Nwin); zeros(50,1)];

% Calculando o espectro de frequência
Whann = fft(whann);

% Calculando o ganho (dB)
WhnndB = -20*log10(abs(Whann));

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(2,2,[1 3])
plot(whann);
grid on;
axis([0 500 0 1.05]);
title('Janela Hann');

subplot(2,2,[2 4])
plot(WhnndB);
grid on;
ylabel('$Gain (dB)$','interpreter','latex','fontsize',fSize);

%%
input('Precione ENTER para continuar...');

fprintf('\n\nJanela Kaiser\n\n');

% Criando a janela
wkai = [zeros(50,1); kaiser(Nwin,15); zeros(50,1)];

% Calculando o espectro de frequência
Wkai = fft(wkai);

% Calculando o ganho (dB)
WkaidB = -20*log10(abs(Wkai));

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(2,2,[1 3])
plot(wkai);
grid on;
axis([0 500 0 1.05]);
title('Janela Kaiser');

subplot(2,2,[2 4])
plot(WkaidB);
grid on;
ylabel('$Gain (dB)$','interpreter','latex','fontsize',fSize);

%%
input('Precione ENTER para continuar...');

fprintf('\n\nJanela Gauss\n\n');

% Criando a janela
wgauss1 = [zeros(50,1); gausswin(Nwin,4); zeros(50,1)];
wgauss2 = [zeros(50,1); gausswin(Nwin,7); zeros(50,1)];

% Calculando o espectro de frequência
Wgauss1 = fft(wgauss1);
Wgauss2 = fft(wgauss2);

% Calculando o ganho (dB)
WgaussdB1 = -20*log10(abs(Wgauss1));
WgaussdB2 = -20*log10(abs(Wgauss2));

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(2,2,[1 3])
plot(wgauss1,'-b'); hold on; plot(wgauss2,'-r');
grid on;
axis([0 500 0 1.05]);
legend('alpha=4','alpha=7');
title('Janela Gauss');

subplot(2,2,[2 4])
plot(WgaussdB1,'-b'); hold on; plot(WgaussdB2,'-r');
grid on;
ylabel('$Gain (dB)$','interpreter','latex','fontsize',fSize);

%%
input('Precione ENTER para continuar...');

fprintf('\n\nJanela Tukey\n\n');

% Criando a janela
wtk1 = [zeros(50,1); tukeywin(Nwin,0.25); zeros(50,1)];
wtk2 = [zeros(50,1); tukeywin(Nwin,0.5); zeros(50,1)];
wtk3 = [zeros(50,1); tukeywin(Nwin,0.75); zeros(50,1)];

% Calculando o espectro de frequência
Wtk1 = fft(wtk1);
Wtk2 = fft(wtk2);
Wtk3 = fft(wtk3);

% Calculando o ganho (dB)
Wtk1dB = -20*log10(abs(Wtk1));
Wtk2dB = -20*log10(abs(Wtk2));
Wtk3dB = -20*log10(abs(Wtk3));

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(2,2,[1 3])
plot(wtk1 ,'-b'); hold on; plot(wtk2 ,'-r'); plot(wtk3 ,'-g');
grid on;
axis([0 500 0 1.05]);
leg=legend('r=0.25','r=0.5','r=0.75');
set(leg,'location','best')
hold off;
title('Janela Tukey');

subplot(2,2,[2 4])
plot(Wtk1dB,'-b'); hold on; plot(Wtk2dB,'-r'); plot(Wtk3dB,'-g');
grid on;
ylabel('$Gain (dB)$','interpreter','latex','fontsize',fSize);