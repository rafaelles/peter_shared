clear all
clc

flagsom = [ ];
fs = 44100;
nbits = 8;
nchannel = 1;

% Tempo de janela de gravacaoo em segundo
twin = 1;

tmax = 5;               %s
conmax = ceil(tmax/twin);

cont = 1;
audiosample = [ ];
stemax = 1;

while isempty(flagsom) == 1 && cont <= conmax
    clear recObj x stemaxaux
    
    recObj = audiorecorder(fs,nbits,nchannel);
    
    recordblocking(recObj,twin);
    x = getaudiodata(recObj);
    
    [flagsom,stemaxaux] = analisasepara(x,stemax,cont);
    stemax = stemaxaux;
    
    audiosample(:,cont) = x';
    cont = cont + 1;
end

clear recObj x stemaxaux

xtot = reshape(audiosample,[ ],1);

time = linspace(0,min(size(audiosample))*twin,length(xtot));

try
    loudness = integratedLoudness(xtot,fs)
    target = -23;
    gaindB = target - loudness;
    gain = 10^(gaindB/20);
    
    xn = xtot.*gain;
    
    plot(time,xn);
    sound(xn,fs)
    audiowrite('ba2n.wav',xn,fs);
catch
    plot(time,xtot);
    sound(xtot,fs)
    audiowrite('ba2.wav',xtot,fs);
end

%%
try
    [x,Fs] = audioread('ba2n.wav');
catch
    [x,Fs] = audioread('ba2.wav');
end

dt = 1/Fs;

t_frame = 25e-3;
N = ceil(t_frame*Fs);

indzero = [ ];
if mod(length(x),N) ~= 0
    Nzeros = N - mod(length(x),N);
    indzero = length(x)+1:length(x)+Nzeros;
    x(indzero) = 0;
end
time = 0:dt:(length(x)-1)/Fs;
Nframes = length(x)/N;

frames = zeros(Nframes,N);
for ii = 1:Nframes
    frames(ii,:) = x(1+(ii-1)*N:ii*N);
end

for ii = 1:Nframes
    ste(ii) = sum(frames(ii,:).^2);
end

ste = ste/max(ste);

indSemSilencio = find(ste>0.01);

framesSemSilencio = frames(indSemSilencio,:);

xSemSilencio = reshape(framesSemSilencio',1,[]);

% Gravando o audio sem silencio
audiowrite('ba2ss.wav',xSemSilencio,Fs);