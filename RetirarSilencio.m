clear all
close all
clc

fSize = 16;

%%
% Entrando com um arquivo de áudio x e sua frequencia de amostragem, Fs
% (Hz)
[x,Fs] = audioread('BaBi.wav');
dt = 1/Fs;

%%
% Para retirar silencio, a ideia eh partir de um janelamento sem
% "overlaping" entre frames. Em médio, o frame de áudio precisa ter, pelo
% menos, 20ms de duração para que se consiga obter um resultado de espectro
% minimamente representativo, sem afetar a percepcao do audio por um ser
% humano.

% Vou escolher um janelamento de t_frame = 25ms = 0.025s. Para descobrir o
% tamanho do frame, N, basta calcular N = t_frame*Fs e arredondar o
% resultado para que o numero de frames seja um numero inteiro

t_frame = 25e-3;
N = ceil(t_frame*Fs);

% funcao ceil( ) arredonda para cima. Ex. ceil(4.1) = 5

% Existe uma necessidade de conferir se o tamanho do audio origial pode ser
% dividido em um numero inteiro de frames. Se o tamanho do audio nao for
% multiplo do tempo do frame, a gente pode fazer uma "gambiarrinha" que se
% chama "zero padding", ou colchão de zeros. Como o nome sugere, a gente
% vai pegar a quantidade de pontos que faltam para deixar o numero de
% frames inteiro e adicionar zeros.

% Exemplo, imagine que o nosso frame tivesse sido calculado e fosse N=4. Ou
% seja, a cada 4 pontos do sinal original, a gente teria um frame. Suponha
% agora que o nosso sinal original seja x = [1 2 3 4 5 6 7 8 9 10 11 12 13]
% com 13 elementos. O numero de vezes que a gente consegue dividir 13/4 de
% maneira inteira eh 3 (3*4=12), mas vai faltar o elemendo 13 que não
% consegue sozinho formar um frame. Para não perder a informacao do
% elemento 13, vamos fazer os frames da seguinte forma:
% frame(1) = [1 2 3 4];
% frame(2) = [5 6 7 8];
% frame(3) = [9 10 10 12];
% frame(4) = [13 0 0 0];
% Assim o sinal ficaria x = [1 2 3 4 5 6 7 8 9 10 11 12 13 0 0 0], com um
% colchão de zeros no final para formar um ultimo frame inteiro

indzero = [ ];
if mod(length(x),N) ~= 0
    % A função mod(x,y) calcula o resto da divisao entre x e y. Exemplo:
    % mod(13,4) -> 13 = 12 + 1 = 3*4 + 1, logo mod(13,4) = 1 porque na
    % divisao de 13 por 4 sobra 1. mod(4,2) = 0 porque 4 eh multiplo de 2.
    Nzeros = N - mod(length(x),N);
    indzero = length(x)+1:length(x)+Nzeros;
    x(indzero) = 0;
end
time = 0:dt:(length(x)-1)/Fs;

% Separando o sinal original em frames
Nframes = length(x)/N;

frames = zeros(Nframes,N);
for ii = 1:Nframes
    frames(ii,:) = x(1+(ii-1)*N:ii*N);
end

%%
% A energia de um sinal é proporcional ao quadrado do sinal, então faremos
% uma estimativa da energia contida em cada frame. STE = short-term energy
% Outra coisa vantajosa é normalizar os valores de energia obtidos pelo
% max(ste), assim a energia estimada está contida entre 0 e 1 (0 e 100%)

for ii = 1:Nframes
    ste(ii) = sum(frames(ii,:).^2);
end

ste = ste/max(ste);


%%
%
if Nframes > 20
    figure('units','normalized','outerposition',[0 0 0.9 0.9],'color','w');
    sh1 = subplot(5,5,[1 2 6 7]);
    h1 = plot(time,x,'-k');
    leg1 = legend('$Sinal$');
    grid on;
    set(h1,'linewidth',1);
    set(leg1,'fontsize',16,'interpreter','latex');
    xlabel('$t (s)$','interpreter','latex','fontsize',fSize);
    ylabel('$x(t)$','interpreter','latex','fontsize',fSize);
    set(gca,'fontsize',fSize);
    Ylimt = get(sh1,'YLim');
    axis([time(1) time(end) Ylimt(1) Ylimt(2)]);
    
    % Vou sortear aleatoriamente um frame para ele ser mostrado na figura a
    % seguir
    % frmsrt = 32;
    frmsrt = 1;
    while frmsrt <= 10 || frmsrt >= Nframes-10
        frmsrt = round(rand*Nframes);
    end
    indfrm = 1+(frmsrt-5)*N:(frmsrt+5+1)*N;
    for ii = 0:11
        timeframe(ii+1) = time(1+(frmsrt-5+ii)*N);
    end
    
    sh2 = subplot(5,5,[4 5 9 10]);
    h2 = plot(time(indfrm),x(indfrm),'-k');
    hold on;
    for ii = 1:12
        line([timeframe(ii) timeframe(ii)],get(sh1,'YLim'),'color',[0 0 1]);
    end
    for ii = 1:11
        strgFrame = [ ];
        strgFrame = ['F' num2str(ii+frmsrt-5)];
        txt2 = text(timeframe(ii)+t_frame/6,Ylimt(2)*0.9,strgFrame);
        set(txt2,'color',[0 0 1]);
    end
    set(h2,'linewidth',1);
    xlabel('$t (s)$','interpreter','latex','fontsize',fSize);
    ylabel('$x(t)$','interpreter','latex','fontsize',fSize);
    set(gca,'fontsize',fSize);
    axis([timeframe(1)-t_frame/2 timeframe(end)+t_frame/2 Ylimt(1) Ylimt(2)]);
    grid on;
    
    sh3 = subplot(5,5,[16 17 21 22]);
    h3 = stem(ste,'xk');
    grid on;
    xlabel('$Frames$','interpreter','latex','fontsize',fSize);
    ylabel('$STE_{n}$','interpreter','latex','fontsize',fSize);
    set(gca,'fontsize',fSize);
    set(h3,'linewidth',1);
    axis([0 Nframes 0 1]);
    
    sh4 = subplot(5,5,[19 20]);
    h4 = stem(frmsrt-4:frmsrt+6,ste(frmsrt-4:frmsrt+6),'xk');
    hold on;
    grid on;
    axis([frmsrt-5 frmsrt+7 0 1]);
    set(h4,'linewidth',1);
    ylabel('$STE_{n}$','interpreter','latex','fontsize',fSize);
    set(gca,'fontsize',fSize);
    
    sh5 = subplot(5,5,[24 25]);
    h5 = stem(frmsrt-4:frmsrt+6,ste(frmsrt-4:frmsrt+6),'xk');
    hold on;
    grid on;
    axis([frmsrt-5 frmsrt+7 0 0.1]);
    set(h5,'linewidth',1);
    xlabel('$Frames$','interpreter','latex','fontsize',fSize);
    ylabel('$STE_{n}$','interpreter','latex','fontsize',fSize);
    set(gca,'fontsize',fSize);
    line([1 Nframes],[0.01 0.01],'color',[1 0 0]);
    annotation('textarrow',[0.76 0.74],[0.16 0.13],'String','1%',...
        'color',[1 0 0]);
    
    clear h1 h2 h3 h4 h5 sh1 sh2 sh3 sh4 sh5 leg1
else
    figure('units','normalized','outerposition',[0 0 0.9 0.9],'color','w');
    sh1 = subplot(2,2,[1 2]);
    h1 = plot(time,x,'-k');
    leg1 = legend('$Sinal$');
    grid on;
    set(h1,'linewidth',1);
    set(leg1,'fontsize',16,'interpreter','latex');
    xlabel('$t (s)$','interpreter','latex','fontsize',fSize);
    ylabel('$x(t)$','interpreter','latex','fontsize',fSize);
    set(gca,'fontsize',fSize);
    Ylimt = get(sh1,'YLim');
    axis([time(1) time(end) Ylimt(1) Ylimt(2)]);
    
    sh2 = subplot(2,2,3);
    h2 = stem(ste,'xk');
    grid on;
    xlabel('$Frames$','interpreter','latex','fontsize',fSize);
    ylabel('$STE_{n}$','interpreter','latex','fontsize',fSize);
    set(gca,'fontsize',fSize);
    set(h2,'linewidth',1);
    axis([0 Nframes 0 1]);
    
    sh3 = subplot(2,2,4);
    h3 = stem(ste,'xk');
    hold on;
    grid on;
    axis([0 Nframes 0 0.1]);
    set(h3,'linewidth',1);
    xlabel('$Frames$','interpreter','latex','fontsize',fSize);
    set(gca,'fontsize',fSize);
    line([0 Nframes],[0.01 0.01],'color',[1 0 0]);
    annotation('textarrow',[0.76 0.74],[0.185 0.15],'String','1%',...
        'color',[1 0 0]);
    
    clear h1 h2 h3 sh1 sh2 sh3 leg1
end
%%
% Agora eu quero procurar apenas os frames que possuem uma energia
% normalizada maior do que 1%, um critério que eu estou adotando ad hoc

indSemSilencio = find(ste>0.01);

framesSemSilencio = frames(indSemSilencio,:);

% Agora juntando os frames sem silencio lado-a-lado e reconstruindo um
% sinal sem silencio

xSemSilencio = reshape(framesSemSilencio',1,[]);

% Gravando o audio sem silencio
audiowrite('BaBi_semSilencio.wav',xSemSilencio,Fs);

%%
% Mostrando graficamente o sinal sem silencio e comparando com o sinal
% original

figure('units','normalized','outerposition',[0 0 0.8 0.8],'color','w');
subplot(1,2,1);
h(1) = plot(time,x,'-k');
grid on;
set(h(1),'linewidth',1);
xlabel('$t (s)$','interpreter','latex','fontsize',fSize);
ylabel('$x(t)$','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([time(1) time(end) Ylimt(1) Ylimt(2)]);
leg1 = legend('$Sinal\,original$');
set(leg1,'fontsize',16,'interpreter','latex');


subplot(1,2,2);
time2 = 0:dt:(length(xSemSilencio)-1)/Fs;
h(2) = plot(time2,xSemSilencio,'-b');
grid on;
set(h(2),'linewidth',1);
xlabel('$t (s)$','interpreter','latex','fontsize',fSize);
ylabel('$x(t)$','interpreter','latex','fontsize',fSize);
set(gca,'fontsize',fSize);
axis([time2(1) time2(end) Ylimt(1) Ylimt(2)]);
leg2 = legend('$Sinal\,sem\,silencio$');
set(leg2,'fontsize',16,'interpreter','latex');

%%
input('Aperte ENTER para ouvir os dois audios (com e sem silencio, respectivamente)...');

sound(x,Fs);
pause(2);
sound(xSemSilencio,Fs);
